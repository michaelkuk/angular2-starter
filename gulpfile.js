'use strict'

const
    path            = require('path'),
    gulp            = require('gulp'),
    browserify      = require('browserify'),
    tsify           = require('tsify'),
    uglify          = require('gulp-uglify'),
    sourcemaps      = require('gulp-sourcemaps'),
    sass            = require('gulp-sass'),
    rename          = require('gulp-rename'),
    autoprefixer    = require('gulp-autoprefixer'),
    source          = require('vinyl-source-stream'),
    buffer          = require('vinyl-buffer'),
    browserSync     = require('browser-sync').create()

const config = require('./config/build.config')

gulp.task('build:scripts', () => {
    let b = browserify({
        entries: path.join(config.scripts.path, config.scripts.main),
        debug: true
    }).plugin(tsify).transform('./config/ng2inlinetransform')

    return b.bundle()
        .pipe(source('bundle.min.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        // .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(config.dest))
})

gulp.task('build:styles', () => {
    return gulp.src(path.join(config.styles.path, config.styles.main))
        .pipe(sass({outputStyle: 'compressed', includePaths: [path.join(__dirname, 'node_modules')]}).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(rename({basename: 'bundle.min'}))
        .pipe(gulp.dest(config.dest))
})

gulp.task('build:index', () => {
    return gulp.src(config.index)
        .pipe(gulp.dest(config.dest))
})

gulp.task('browser-sync', () => {
    browserSync.init({
        server: {
            baseDir: path.join(__dirname, config.dest)
        }
    });
})

gulp.task('watch', () => {
    gulp.watch([
        '!src/main.scss',
        '!src/index.html',
        'src/**/*',
    ], ['build:scripts'])

    gulp.watch([config.index], ['build:index'])
    gulp.watch([path.join(config.styles.path, config.styles.main)], ['build:styles'])
})

gulp.task('build', ['build:scripts', 'build:styles', 'build:index'])

gulp.task('default',['build', 'browser-sync', 'watch'])
