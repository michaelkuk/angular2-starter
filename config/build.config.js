
module.exports = {
    scripts: {
        path: "src",
        main: "main.ts"
    },
    styles: {
        path: 'src',
        main: 'main.scss',
        autoprefixer: {

        }
    },
    index: 'src/index.html',
    dest: 'dist'
}
